# VISUALGRAPHX - 0.0.1

## About
We developed VISUALGRAPHX, a web-based visualization tool for large-scale graphs. 
Current graph visualization tools that follow the rich-internet paradigm lack an interactive 
and scalable visualization of graph-based data. VISUALGRAPHX aims to provide a 
universal graph visualization tool that empowers the users to efficiently explore the data 
for themselves at a large scale. It has been incorporated into the galaxy platform as an 
visualization plugin that is directly accessible through the User History.

## Install
Visualizations in galaxy are located in the default folder `/config/plugins/visualizations/` that 
needs to be specified in `/config/galaxy.ini` at `visualization_plugins_directory`. VISUALGRAPHX 
needs to be located in this folder in order to be loaded on startup in Galaxy. More information
can be found at the [Visualizations Registry](https://wiki.galaxyproject.org/VisualizationsRegistry) 
in the official Galaxy documentation. VISUALGRAPHX can be copied to the desired folder by cloning 
the gitlab repository when utilizing the git command `git-clone`.

```
cd ~/pathtogalaxy/config/plugins/visualizations/
git clone gitlab.com/comptrans/VisualGraphX.git
```

Furthermore, we provide a gruntfile (`gruntfile.js`) to configure [GRUNT](http://gruntjs.com/) for 
the automation of this process. GRUNT is a generic task runner for the JavaScript runtime environment 
[Node.js](https://nodejs.org) to streamline repetitive tasks and can be installed through the 
[Node.js package manager (npm)](https://www.npmjs.com/). For installation instructions, please read 
the official [documentation](http://gruntjs.com/getting-started) for GRUNT. At first, install the
dependencies that are specified in the `package.json` with the command `npm install`. Next, modify
the path to Galaxy by editing line 7 in the gruntfile.

```
galaxy: 'pathtogalaxy',
```

This ensures that VISUALGRAPHX is copied into the correct folder. Type `grunt` in the command line to
initiate the copy process. Finally, restart Galaxy to load VISUALGRAPHX.

## Implementation
VISUALGRAPHX has been written in JavaScript and utilizes the d3.js library for the interactive graph 
visualization. It implements the Model-View-Controller concept by usage of the Backbone.js library.
Furthermore, DefiantJS allows the efficient search of large JSON files using XPath expressions.

## Supported Internet Browsers
VisualGraphX has been tested with Chrome and Firefox is its recent versions.

## File Format
JSON (JavaScript Object Notation) in node-link format is the desired input file format. 
In its most minimal form it requires arrays for both the `nodes` and the `links`. In particular, 
`nodes` contains objects that correspond to the nodes in the graph with arbitraty attributes.
However, the objects have to include the attribute `id` whose value needs to be unique among 
the objects. Other attributes that can be included in the visualization are the node label (`name`),
and URL of external graphic (`chart`). Similarly, the `links` array contains objects that correspond
to the links/edges in the graph with the attributes `source` and `target` whose values are the id's 
defined in the node objects. A most minimal input file for VISUALGRAPH is shown below that creates
a graph with two connected nodes. 

```javascript
{
	"nodes": [
		{id: 0},
		{id: 1}
	],
	"links": [
		{source: 0, target: 1}
	]
}
```

### Note 
Make sure that Galaxy recognizes the input file as JSON, otherwise it cannot be visualized.

## Usage
VISUALGRAPHX comes with the required files for its integration into the Galaxy Visualization Registry.
Select a JSON dataset from the history panel and click on the `Visualize` icon and select VisualGraphX.
Now, the configuration panel appears in which one can adjust the parameters for the graph visualization.
Clicking on `Visualize` initiates the visualization. Finally, the `Export` allows to export the graph
either as scalable vector graphics (SVG) or portable network graphics (PNG). 

## Conversion between the Graph Formats
VISUALGRAPHX aims to be a generic approach for the visualization of graphs. Therefore, we provide a converter 
for Galaxy that takes as input a simple interaction format (SIF) file with an optional attributes list in 
tabular format and converts it to a JSON file in the node-link format. It can be installed directly through the 
[Galaxy Tool Shed](https://toolshed.g2.bx.psu.edu/repository?repository_id=15a6add958eae279&changeset_revision=eb471a967693)
(repository: vgx_converter). Data from [CBN](http://www.causalbionet.com) can be exported as SIF file and visualized with 
VISUALGRAPHX when being converted to JSON.  

## Data
We provide several example data that can be used with VISUALGRAPHX and can be found under the `sample data` directory. 
`/sample data/CoVennTree.json` is a dataset that results from analysis with 
[CoVennTree](https://toolshed.g2.bx.psu.edu/view/steffen/covenntree/745aede829e9) and further conversion with SIF-to-VGX as 
explained above. It was originally described in [Steglich et. al (2014)](http://www.ncbi.nlm.nih.gov/pubmed/25450165/). Furthermore, 
the angiogenesis network that has been exported from the [Causal Biological Network Database](http://www.causalbionet.com) to illustrate
the general applicability of VISUALGRAPHX. It can be found at `/sample data/Angiogenesis.sif` with the converted file 
`/sample data/Angiogenesis.json`. Additionally, `/sample data/mTOR_ChEMBL.sif` and `/sample data/mTOR_ChEMBL.json`is a network that has 
been exported from [ChEMBL](https://www.ebi.ac.uk/chembl/) in Cytoscape and consists of 4692 nodes with 7688 edges. 


## Troubleshooting
As a first measure, ensure that Galaxy loaded VISUALGRAPHX successfully. Check the logile of Galaxy 
and search for the lines that start with `galaxy.web.base.pluginframework`. Make sure that VISUALGRAPHX
is among the loaded plugins. There should be a line similar to the following:

```
galaxy.web.base.pluginframework INFO 2016-02-15 10:56:22,177 VisualizationsRegistry, loaded plugin: visualgraphx
```

If there is an error message, it should look similar to this:

```
galaxy.web.base.pluginframework WARNING 2016-02-15 10:56:22,177 VisualizationsRegistry, plugin load failed or disabled:
./config/plugins/visualizations/visualgraphx. Skipping...
```

If VISUALGRAPHX has been loaded but still fails to start when selecting `VisualgraphX` on the `Visualize`
icon on the history panel then enable the debugging output. Change to the visualgraphx directory in 
the `./config/plugin/visualization/` and comment the first two lines of `./static/app.js`. 

```javascriopt
//window.console.debug = function() {};
//window.console.log = function() {};
```

Now, select `visualgraphx` over the `Visualize` icon and open the the console in chrome or firefox and
create a new issue and paste the output of the console. 