// dependencies
define(['plugin/graph/d3js/common/config'], function(d3js_config) {
        return $.extend(true, {}, d3js_config, {
            title: 'covenntree',
            category: 'Phylogenetic Networks'
        });
});
