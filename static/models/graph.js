// dependencies
define([], function() {
        //
        var node = Backbone.Model.extend({
            defaults: {
                id: '',
                expanded: false
            }
        });

        var link = Backbone.Model.extend({
            defaults: {
                source: '',
                target: ''
            }
        });

        var setup = Backbone.Model.extend({
            defaults: {
                source: '',
                source_prop: '',
                label_node: '',
                rootnode: '',
                max_depth: '',
                set_depth: ''
            }
        });

        var nodes = Backbone.Collection.extend({
            model: node
        });

        var links = Backbone.Collection.extend({
            mode: link
        });

        return Backbone.Model.extend({

            // defaults
            defaults : {
                id: null,
                title: '',
                type: 'd3js_generic',   // d3js_generic as the default (predefined) graph type
                definition: '', // e.g. category
                date: null,
                state: '',
                state_info: '',
                modified: false,
                dataset_id: '',
                dataset_id_job : ''
            },

            // initialize
            initialize: function(app, options) {
                var _this = this;
                this.app = app;

                // initialize models for nodes and links
                this.nodes = new nodes();
                this.links = new links();

                // initialize model for the visualization setup (e.g. rootnode, depth)
                this.setup = new setup();

                // set definition of the graph as specified in
                this.set('definition', this.app.types.attributes['d3js_generic']);

                this.listenTo(this, 'sync', function() {

                    this.nodes.add(this.get('nodes'));
                    this.links.add(this.get('links'));

                    this.properties = {};
                    this.properties.nodes = this._properties(this.nodes.toJSON());
                    this.properties.links = this._properties(this.links.toJSON());

                    // save graphdata in JSON object
                    this.data = {};
                    this.data.nodes = this.nodes.toJSON();
                    this.data.links = this.links.toJSON();

                    /* ################# SET initial graph.setup values ##################*/

                    // determine (default) rootnode (as lowest id) and depth of graph beginning with rootnode
                    var rootnode = Math.min.apply(Math, this.nodes.toJSON().map(function(node) {return node.id;}));
                    //var depth = this._graphdepth(this.data, rootnode, []);
                    var depth = 100; // default depth

                    // set predefined values
                    this.setup.set('source', 0);
                    this.setup.set('source_prop', this._properties_preselect(this.properties.nodes,'chart')); // default

                    // set default (node) label: name if present otherwise first attribute
                    this.setup.set('label_node', this._properties_preselect(this.properties.nodes,'name'));

                    this.setup.set('rootnode', rootnode);
                    this.setup.set('max_depth', depth);
                    this.setup.set('set_depth', (depth >= 2) ? 2 : depth );

                    this.trigger('ready', this); // fire 'ready'-event - meaning that the data has been fetched

                });
                this.fetch();
            },

            url: function() {
                return this.app.options.durl;
            },

            // determine the depth of the graph
            /*_depth: function(rootnode, adjlist) {
                var nodelist = [];
                var completelist = [];
                var next = adjlist[rootnode];
                counter = 0;

                if(nodelist.length == 0) {
                    return counter;
                } else {
                    nodelist = next;
                    while(next != 0) {
                        counter = counter + 1;
                        for(key in nodelist) {
                            next.push(adjlist[nodelist[key]]);
                        }

                    }
                }
                console.log(counter);
            },*/

            // determine adjacency list
            _adj: function(data, adj) {
                for(key in data.nodes) {
                    adj[data.nodes[key].id] = []; // intialize nodes
                }
                for(key in data.links) {
                    adj[data.links[key].source].push(data.links[key].target);
                }
            },

            // determines the depth of a graph recursively
            _graphdepth: function(data, rootnode, path) {
                var childs = JSON.search(data, '//links[source="' + rootnode + '"]/target');

                if(childs.length == 0) {
                    return 0;
                } else {
                    path.push(rootnode); // remember active path
                    var sub = [];
                        for(j in childs) {
                            // check if child is in active path: backedge
                            if(path.indexOf(childs[j]) == -1) {
                                sub.push(this._graphdepth(data, childs[j], path) +1);
                            }
                        }
                        return Math.max(...sub);
                }
            },

            // get list of properties
            _properties: function(col) {
                var keys = Object.keys(col[0]);
                var props = [];
                for(var i=0; i < keys.length; i++) {
                    props[i] = {prop: keys[i]};
                }
                return props;
            },

            // determine a preselection in propery if available
            _properties_preselect: function(props, searchfor) {
                var presel = props[0].prop;
                for(prop in props) {
                    if(props[prop].prop.indexOf(searchfor) > -1) {
                        presel = searchfor;
                    }
                }
                return presel;
            }
    });
});

 /*
                if(childs.length == 0) {
                    return 0;
                } else {
                    var sub = [];
                    for(var j in childs) {
                        console.log(childs[j]);
                        if(visited.indexOf(childs[j]) > -1){
                            console.log('already visited');
                        }
                        if(visited.indexOf(childs[j]) == -1) {
                            sub.push(this._graphdepth(data, childs[j], visited) +1);
                            visited.push(childs[j]);
                        }
                    }
                    return Math.max(...sub);
                }*/


                 //  verify the para metrized depth of the graph
            /*_verifydepth : function(data, rootnode, depth) {
                var counter = 0; // counter for the depth
                var verification = false;
                console.log(data);

                // create toplogy of graph as list
                var top = [[rootnode]];

                // iterate through the levels
                while(counter <= depth) {
                    for(tier in top) {
                        for(node in tier) {

                        }
                        top[counter]
                    }
                }
            },/*

            // test for
            _connectivity: function() {

            },

            /*
            //implementation of the iterative deepening depth first search
            _iddfs: function(root) {
                var depth = 1000;
                var found;
                for(var i=0; i <= depth; i++) {
                    found = this_dls(root, depth);
                    if(found) {
                        return;
                    }
                }
                var found = this._dls();
            },


            // implementation of the recursive depth-limited DFS
            _dls: function(node, depth) {
                if(depth == 0) {
                    return;
                }
            },*/
