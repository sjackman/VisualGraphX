// import dependencies
define(['mvc/ui/ui-tabs',
    'mvc/ui/ui-misc',
    'mvc/ui/ui-portlet',
    'utils/utils',
    'plugin/views/editor/start',
    'plugin/views/editor/settings',
    'plugin/views/types'],
    function(Tabs, Ui, Portlet, Utils, StartTab, SettingsTab, Types) {
    /* ########## VIEW4 ########## */
    return Backbone.View.extend({
            // initialize
            initialize: function(app, options) {

                var _this = this; // linkage of original this (view4)
                console.debug('editor.js:: initialize() - link this (view4) in _this');
                 /* link the app (view1) for method invocation without parameters in editor.js*/
                this.app = app;
                console.debug('editor.js:: initialize() - link the app (view1) in this.app')
                // create message element from ui-misc
                this.message = new Ui.Message(); // cid = "view3"
                console.debug('editor.js:: initialize() - link ui-message (view3) in this.message');

                // create the portlet that functions as canvas for the editor
                this.portlet = new Portlet.View({
                    icon: 'fa fa-pencil-square-o',
                    title: 'VisualGraphX - Interactive Graph Visualization',
                    operations: {
                        'save': new Ui.ButtonIcon({
                            tooltip: 'Visualize the Graph',
                            title: 'Visualize',
                            icon: 'fa fa-long-arrow-right',
                            onclick: function() {
                                _this.app.go('viewer');
                            }
                        })
                    }
                });
                console.debug('editor.js:: initialize() - link portlet (view9) in this.portlet');

                // ########## TABS ##########
                // create the tabs on editor screen
                this.tabs = new Tabs.View({});
                console.debug('editor.js:: initialize() - link tabs (view10) in this tabs');

                // ########## START ##########
                this.start = new StartTab(app);
                $start = this.start.$el;

                // create the tab: "start" itself
                this.tabs.add({
                    id: 'start',
                    title: 'Start',
                    $el: $start
                });

                /*
                this.listenTo(this.app, 'startup', function() {
                    console.log('bla something');
                    this.settings = new SettingsTab(app);
                    $settings = this.settings.$el;
                    this.tabs.add({
                        id: 'settings',
                        title: 'Settings'
                    });
                });*/

                // ########## SETTINGS ##########
                this.settings = new SettingsTab(app);
                $settings = this.settings.$el; // create the tab: "Settings" itself
                this.tabs.add({
                    id: 'settings',
                    title: 'Settings',
                    $el: $settings
                });

                // fetch message and tab elements to portlet
                this.portlet.append(this.message.$el.addClass('ui-margin-top'));
                this.portlet.append(this.tabs.$el);

                // override editor.js el with portlet el
                this.setElement(this.portlet.$el);
                this.reset(); // reset the settings for the graph

            }, // initialize


            // show
            show: function() {
                this.$el.show();
            }, // end show

            // hide
            hide: function() {
                this.$el.hide();
            }, // end hide

            // reset the graph
            reset: function() {
                this.app.graph.set('type', 'd3js_generic'); // set d3js_generic as default graph type
                this.app.graph.set('dataset_id', this.app.options.config.dataset_id);
                console.debug('editor.js:: reset() - ');
            }
    }); // end Backbone.View.extend
}); // define



